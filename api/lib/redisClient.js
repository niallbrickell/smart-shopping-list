import promiseRedis from 'promise-redis';

const redis = promiseRedis();

const client = redis.createClient();

client.on('error', (error) => {
  throw new Error(error);
});

export default client;
