import 'babel-polyfill';
import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from 'koa-cors';
import registerActions from './actions/index';

const actions = {};

export default function registerAction(name, func) {
  actions[name] = func;
}

registerActions(registerAction);

const app = new Koa();

app.use(cors({
  origin: '*',
  methods: ['GET', 'POST', 'OPTIONS'],
}));

app.use(bodyParser());

app.use(async (ctx, next) => {
  const splitURL = ctx.url.split('?')[0].split('/');

  if (splitURL[1] === 'call') {
    const action = actions[splitURL[2]];

    if (action) {
      let result;
      try {
        result = await action(ctx.request.body);
      } catch (e) {
        if (e && e.redirect) ctx.redirect(e.redirect);
        else {
          ctx.throw(e.status || 500, e);
          return;
        }
      }

      if (result.status && result.message) {
        ctx.throw(result.status || 500, result);
        return;
      }

      ctx.body = result; // eslint-disable-line no-param-reassign
      next();
    } else {
      ctx.throw(404, 'NOT FOUND');
    }
  }
});

app.listen(3001, (err) => {
  if (err) console.error(err);
  console.info('----\n==> 🌎  API is running on port %s', 3001);
  console.info('==> 💻  Send requests to http://%s:%s', 'localhost', 3001);
});
