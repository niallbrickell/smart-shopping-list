import getListFromToken from './getListFromToken';
import storeList from './storeList';
import emailList from './emailList';

export default function register(registerAction) {
  registerAction('getListFromToken', getListFromToken);
  registerAction('storeList', storeList);
  registerAction('emailList', emailList);
}
