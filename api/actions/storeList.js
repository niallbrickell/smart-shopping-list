import crypto from 'crypto';
import redisClient from '../lib/redisClient';

export default async function storeList({ data }) {
  const buffer = await crypto.randomBytes(48);
  const token = buffer.toString('base64').substring(0, 10).replace('/', '');

  redisClient.set(token, JSON.stringify(data));

  // Expire token in a month
  redisClient.expireat(token, parseInt((+new Date()) / 1000, 10) + 2.628e+6);

  return { token };
}
