import storeList from '../storeList';
import getListFromToken from '../getListFromToken';
import redisClient from '../../lib/redisClient';

describe('(API)', () => {
  describe('(Actions)', () => {
    let token;
    const items = {
      fresh: ['oranges', 'pears', 'potatoes'],
      bakery: ['baguette', 'cookies'],
    };

    afterAll(() => {
      if (token) redisClient.del(token);
    });

    describe('Store list', () => {
      it('Should return a token for a JSON items list', async () => {
        const response = await storeList({ data: items });
        token = response.token;
        expect(typeof token).toBe('string');
      });
    });

    describe('Retreive list', () => {
      it('Should return an object when given a token.', async () => {
        const response = await getListFromToken({ token });
        const data = response.data;
        expect(data).toEqual(items);
      });
    });
  });
});
