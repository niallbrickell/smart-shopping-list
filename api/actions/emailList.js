import nodemailer from 'nodemailer';
import mailgunTransport from 'nodemailer-mailgun-transport';

export default async function emailList({ email, url }) {
  const auth = {
    auth: {
      api_key: 'key-30ff8ddcf846d9820d797109b06bf302',
      domain: 'smart-shopping.niallbrickell.co.uk',
    },
  };

  const nodemailerMailgun = nodemailer.createTransport(mailgunTransport(auth));

  return await new Promise((resolve, reject) => {
    nodemailerMailgun.sendMail({
      from: 'send@smart-shopping.niallbrickell.co.uk',
      to: email,
      subject: 'Your Shopping List',
      'h:Reply-To': 'donotreply@smart-shopping.niallbrickell.co.uk',
      html:
      `<a href="${url}">Click here to access your list.</a><br />
      Alternatively, copy and paste the url into your browser's address bar: ${url}`,
      text: `Click here to access your list: ${url}`,
    }, (error, info) => {
      if (error) reject({ error });
      else resolve({ info });
    });
  });
}
