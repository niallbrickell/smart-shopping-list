import redisClient from '../lib/redisClient';

export default async function getListFromToken({ token }) {
  if (!token || typeof token !== 'string') {
    return {
      status: 400,
      message: 'Token should be a string',
    };
  }

  const data = await redisClient.get(token);

  if (!data) {
    return {
      status: 404,
      message: 'Token not found',
    };
  }

  return { data: JSON.parse(data) };
}
