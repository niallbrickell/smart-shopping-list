import { combineReducers } from 'redux';

// -- Reducers --
import { ItemsReducer as items } from './modules/Items/Items';
import { AddItemReducer as addItem } from './modules/AddItem/AddItem';
import { SendEmailReducer as sendEmail } from './modules/SendEmail/SendEmail';

export default combineReducers({
  items,
  addItem,
  sendEmail,
});
