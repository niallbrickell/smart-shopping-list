import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './rootReducer';

export default () => {
  // -- Enhancers --
  const enhancers = [];
  if (process.env.NODE_ENV === 'development' && window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  // -- Create store with middleware and enhancers --
  return createStore(rootReducer, compose(
    applyMiddleware(thunkMiddleware),
    ...enhancers
  ));
};
