import { loadData } from './modules/Items/Items';

/**
 * Used to load the initial data from the server.
 */
export default ({ dispatch }) => {
  const token = window.location.href.split('/').slice(-1)[0];
  if (!token) return;
  dispatch(loadData(token));
};
