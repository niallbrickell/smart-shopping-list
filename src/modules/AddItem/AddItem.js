/**
 * Public API for the AddItem module.
 *
 * We use public APIs to limit access to our module.
 * No file can import a dependency from AddItem that isn't listed here.
 * Modules can consist of a component (with child components if required), or
 *  a reducer, logic etc.
 */

// -- Main component --
export { default } from './AddItemContainer';

// -- Reducer --
export { default as AddItemReducer } from './AddItemReducer';

// -- Actions, action types --
export { setDirty, ADD_ITEM } from './AddItemActions';
