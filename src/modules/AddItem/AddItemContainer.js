import { connect } from 'react-redux';

// -- Component --
import AddItemComponent from './AddItemComponent';

// -- Event handlers --
import {
  checkForEnter,
  itemInputChanged,
} from './AddItemEvents';

// -- Connect react to redux --
const mapStateToProps = state => ({
  itemInputValue: state.addItem.itemInputValue,
  pristine: state.addItem.pristine,
});

const mapDispatchToProps = {
  checkForEnter,
  itemInputChanged,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddItemComponent);
