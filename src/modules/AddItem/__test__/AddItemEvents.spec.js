import {
  itemInputChanged,
  checkForEnter,
} from '../AddItemEvents';
import {
  clearItemInput,
  ITEM_INPUT_CHANGED,
  CLEAR_ITEM_INPUT,
} from '../AddItemActions';
import { addItem } from '../../Items/Items';

const ENTER_KEYCODE = 13;

describe('(Modules)', () => {
  describe('(AddItems - Events)', () => {
    it('Should return a correct action when itemInputChanged is called.', () => {
      const fakeItemInputVal = 'fake';
      const fakeEvent = {
        target: {
          value: fakeItemInputVal,
        },
      };

      const returned = itemInputChanged(fakeEvent);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(ITEM_INPUT_CHANGED);
      expect(returned.itemInputValue).toBe(fakeItemInputVal);
    });

    it('Should dispatch addItem and clearItemInput when checkForEnter is called with an enter event.', () => {
      const fakeItemInputVal = 'fake';
      const fakeEvent = {
        keyCode: ENTER_KEYCODE,
        target: {
          value: fakeItemInputVal,
        },
      };

      const fakeDispatch = jest.fn();

      checkForEnter(fakeEvent)(fakeDispatch);

      expect(fakeDispatch).toHaveBeenCalledTimes(2);
      expect(fakeDispatch).toBeCalledWith(clearItemInput());
    });

    it('Should NOT dispatch addItem and clearItemInput when checkForEnter is called without an enter event.', () => {
      const returned = clearItemInput();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(CLEAR_ITEM_INPUT);
    });
  });
});
