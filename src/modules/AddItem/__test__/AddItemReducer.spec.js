import addItemReducer from '../AddItemReducer';
import {
  ITEM_INPUT_CHANGED,
  CLEAR_ITEM_INPUT,
} from '../AddItemActions';
import { SET_DIRTY } from '../../Items/Items';

describe('(Modules)', () => {
  describe('(Items - Reducer)', () => {
    let state;

    const initialState = {
      itemInputValue: '',
      pristine: true,
    };

    it('Should export a function as default.', () => {
      expect(typeof addItemReducer).toBe('function');
    });

    it('Should return an initialState when no action or state is present.', () => {
      state = addItemReducer();

      expect(state).toEqual(initialState);
    });

    it('Should update the state correctly on ITEM_INPUT_CHANGED.', () => {
      const fakeItemInputVal = 'fake';
      const fakeChangedAction = {
        type: ITEM_INPUT_CHANGED,
        itemInputValue: fakeItemInputVal,
      };

      state = addItemReducer(state, fakeChangedAction);

      expect(state.itemInputValue).toBe(fakeItemInputVal);
    });

    it('Should update the state correctly on CLEAR_ITEM_INPUT.', () => {
      const fakeClearAction = {
        type: CLEAR_ITEM_INPUT,
      };

      state = addItemReducer(state, fakeClearAction);

      expect(state).toEqual({
        ...initialState,
        pristine: false,
      });
    });

    it('Should update the state correctly on SET_DIRTY.', () => {
      const fakeClearAction = {
        type: SET_DIRTY,
      };

      const updatedState = addItemReducer(initialState, fakeClearAction);

      expect(updatedState).toEqual({
        ...initialState,
        pristine: false,
      });
    });
  });
});
