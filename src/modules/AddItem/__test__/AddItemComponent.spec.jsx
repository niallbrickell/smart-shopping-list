import React from 'react';
import { shallow } from 'enzyme';
import TextField from 'material-ui/TextField';

import AddItemComponent from '../AddItemComponent';

describe('(Modules)', () => {
  describe('(AddItems - Component)', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(
        <AddItemComponent
          itemInputValue=""
          pristine
          checkForEnter={() => null}
          itemInputChanged={() => null}
        />
      );
    });

    it('Should render a text field.', () => {
      expect(wrapper.find(TextField).length).toBe(1);
    });
  });
});

