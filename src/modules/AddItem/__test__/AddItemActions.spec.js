import {
  itemInputChanged,
  clearItemInput,

  ITEM_INPUT_CHANGED,
  CLEAR_ITEM_INPUT,
} from '../AddItemActions';

describe('(Modules)', () => {
  describe('(AddItems - Actions)', () => {
    it('Should return a correct action when itemInputChanged is called.', () => {
      const fakeItemInputVal = 'fake';

      const returned = itemInputChanged(fakeItemInputVal);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(ITEM_INPUT_CHANGED);
      expect(returned.itemInputValue).toBe(fakeItemInputVal);
    });

    it('Should return a correct action when clearItemInput is called.', () => {
      const returned = clearItemInput();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(CLEAR_ITEM_INPUT);
    });
  });
});
