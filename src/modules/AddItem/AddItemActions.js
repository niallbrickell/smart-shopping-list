export const ITEM_INPUT_CHANGED = 'ITEM_INPUT_CHANGED';
export const CLEAR_ITEM_INPUT = 'CLEAR_ITEM_INPUT';

export const itemInputChanged = itemInputValue => ({
  type: ITEM_INPUT_CHANGED,
  itemInputValue,
});

export const clearItemInput = () => ({
  type: CLEAR_ITEM_INPUT,
});
