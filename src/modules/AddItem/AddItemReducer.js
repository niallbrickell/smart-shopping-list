import {
  ITEM_INPUT_CHANGED,
  CLEAR_ITEM_INPUT,
} from './AddItemActions';
import { SET_DIRTY } from '../Items/Items';

const ACTION_HANDLERS = {
  [ITEM_INPUT_CHANGED]: (state, action) => ({
    ...state,
    itemInputValue: action.itemInputValue,
  }),

  [CLEAR_ITEM_INPUT]: state => ({
    ...state,
    itemInputValue: '',
    pristine: false, // As now we've added an item
  }),

  [SET_DIRTY]: state => ({
    ...state,
    pristine: false,
  }),
};

const initialState = {
  itemInputValue: '',
  pristine: true,
};

// -- Reducing function --
export default (state = initialState, action) => {
  if (!action || !action.type) return state;

  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};
