import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';

import classes from './AddItem.scss';

const AddItemComponent = ({
  itemInputValue,
  pristine,

  checkForEnter,
  itemInputChanged,
}) => (
  <div className={`${classes.addItem} ${pristine ? '' : classes.active}`}>
    <div className={classes.title}>
      {"Let's go shopping!"}
    </div>
    <div className={classes.description}>
      {"Quick enter any product and we'll sort your shopping list in location order"}
    </div>
    <div className={classes.addInput}>
      <TextField
        autoFocus
        value={itemInputValue}
        onChange={itemInputChanged}
        fullWidth
        underlineShow={false}
        name="addItem"
        placeholder="What do you need?"
        inputStyle={{
          fontSize: '1.3rem',
          fontWeight: '300',
          padding: '0 1rem',
          boxSizing: 'border-box',
        }}
        onKeyDown={checkForEnter}
      />
    </div>
  </div>
);

AddItemComponent.propTypes = {
  itemInputValue: PropTypes.string.isRequired,
  pristine: PropTypes.bool.isRequired,

  checkForEnter: PropTypes.func.isRequired,
  itemInputChanged: PropTypes.func.isRequired,
};

export default AddItemComponent;
