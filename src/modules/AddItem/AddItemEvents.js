import {
  clearItemInput,
  itemInputChanged as itemInputChangedAction,
} from './AddItemActions';

import { addItem } from '../Items/Items';

export const itemInputChanged = e => itemInputChangedAction(e.target.value);

export const checkForEnter = e => (dispatch) => {
  const keyCode = e.keyCode ? e.keyCode : e.which;
  if (keyCode === 13) {
    // -- Enter pressed - add the item --
    dispatch(addItem(e.target.value));
    dispatch(clearItemInput());
  }
};
