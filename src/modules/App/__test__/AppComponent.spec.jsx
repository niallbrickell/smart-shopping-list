import React from 'react';
import { shallow } from 'enzyme';

import AppComponent from '../AppComponent';
import AddItem from '../../AddItem/AddItem';
import ItemsList from '../../Items/Items';
import SendEmail from '../../SendEmail/SendEmail';

describe('(Modules)', () => {
  describe('(App)', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(<AppComponent />);
    });

    it('Should render an AddItemComponent.', () => {
      expect(wrapper.find(AddItem).length).toBe(1);
    });

    it('Should render an ItemsList.', () => {
      expect(wrapper.find(ItemsList).length).toBe(1);
    });

    it('Should render a SendEmail.', () => {
      expect(wrapper.find(SendEmail).length).toBe(1);
    });
  });
});
