import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AddItem from '../AddItem/AddItem';
import ItemsList from '../Items/Items';
import SendEmail from '../SendEmail/SendEmail';
import classes from './App.scss';

const AppComponent = () => (
  <MuiThemeProvider className={classes.app}>
    <div>
      <AddItem />
      <ItemsList />
      <SendEmail />
    </div>
  </MuiThemeProvider>
);

export default AppComponent;
