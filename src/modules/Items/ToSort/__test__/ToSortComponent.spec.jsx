import React from 'react';
import { shallow } from 'enzyme';

import ToSortComponent from '../ToSortComponent';

describe('(Modules)', () => {
  describe('(Items)', () => {
    describe('(ToSort - Component)', () => {
      const toSort = ['test', 'fake'];
      let wrapper;
      beforeEach(() => {
        wrapper = shallow(
          <ToSortComponent
            toSort={toSort}
          />
        );
      });

      it('Should render a set amount of items.', () => {
        expect(wrapper.findWhere(n => n.type() === 'p').length).toBe(toSort.length);
      });
    });
  });
});

