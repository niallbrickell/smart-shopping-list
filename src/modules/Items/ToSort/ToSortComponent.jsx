import React, { PropTypes } from 'react';

import classes from './ToSort.scss';

const ToSortComponent = ({ toSort }) => (
  <div className={classes.toSort}>
    { toSort.length ? <h4>Sorting...</h4> : null }
    {
      toSort.map((item, index) => <p key={index}>{item}</p>)
    }
  </div>
);

ToSortComponent.propTypes = {
  toSort: PropTypes.arrayOf(PropTypes.string),
};

export default ToSortComponent;
