import {
  addItem,
  addToSort,
  removeToSort,
  setDirty,

  ADD_ITEM,
  ADD_TO_SORT,
  REMOVE_TO_SORT,
  SET_DIRTY,
} from '../ItemsActions';

describe('(Modules)', () => {
  describe('(Items - Actions)', () => {
    it('Should export addItem as a function.', () => {
      expect(typeof addItem).toBe('function');
    });

    it('Should export addToSort as a function.', () => {
      expect(typeof addToSort).toBe('function');
    });

    it('Should export removeToSort as a function.', () => {
      expect(typeof removeToSort).toBe('function');
    });

    it('Should export setDirty as a function.', () => {
      expect(typeof setDirty).toBe('function');
    });

    it('Should return a correct action when addItem is called.', () => {
      const fakeItem = {
        item: 'test',
        location: 'testLocation',
      };

      const returned = addItem(fakeItem.item, fakeItem.location);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(ADD_ITEM);
      expect(returned.item).toBe(fakeItem.item);
      expect(returned.location).toBe(fakeItem.location);
    });

    it('Should return a correct action when setDirty is called.', () => {
      const returned = setDirty();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(SET_DIRTY);
    });

    it('Should return a correct action when addToSort is called.', () => {
      const fakeItem = {
        item: 'test',
        id: 0,
      };

      const returned = addToSort(fakeItem.item, fakeItem.id);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(ADD_TO_SORT);
      expect(returned.item).toBe(fakeItem.item);
      expect(returned.id).toBe(fakeItem.id);
    });

    it('Should return a correct action when removeToSort is called.', () => {
      const fakeItemId = 0;

      const returned = removeToSort(fakeItemId);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(REMOVE_TO_SORT);
      expect(returned.id).toBe(fakeItemId);
    });
  });
});
