import React from 'react';
import { shallow } from 'enzyme';

import ItemsComponent from '../ItemsComponent';
import SortableList from '../SortableList/SortableList';
import ToSort from '../ToSort/ToSort';

describe('(Modules)', () => {
  describe('(Items - Component)', () => {
    const toSort = ['test', 'fake'];

    let wrapper;
    beforeEach(() => {
      wrapper = shallow(
        <ItemsComponent
          toSort={toSort}
        />
      );
    });

    it('Should render a SortableList.', () => {
      expect(
        wrapper.contains(<SortableList />)
      ).toBe(true);
    });

    it('Should render a ToSortList.', () => {
      expect(
        wrapper.contains(<ToSort toSort={toSort} />)
      ).toBe(true);
    });
  });
});
