import {
  ADD_ITEM,
  ADD_TO_SORT,
  REMOVE_TO_SORT,
  SET_ITEMS_STATE,
} from '../ItemsActions';
import { SET_ITEMS } from '../SortableList/SortableList';
import itemsReducer from '../ItemsReducer';

describe('(Modules)', () => {
  describe('(Items - Reducer)', () => {
    let state;

    let initialState;

    beforeEach(() => {
      initialState = {
        items: {
          fresh: [],
          bakery: [],
          uncategorised: [],
        },
        toSort: {},
      };
    });

    it('Should export a function as default.', () => {
      expect(typeof itemsReducer).toBe('function');
    });

    it('Should return an initialState when no action or state is present.', () => {
      state = itemsReducer();

      expect(state).toEqual(initialState);
    });

    it('Should update the state correctly on ADD_ITEM', () => {
      const fakeLocation = Object.keys(initialState.items)[0];
      const fakeAddAction = {
        type: ADD_ITEM,
        item: 'test',
        location: fakeLocation,
      };

      state = itemsReducer(state, fakeAddAction);

      expect(state.items[fakeLocation]).toContain(fakeAddAction.item);
    });

    it('Should update the state correctly on SET_ITEMS', () => {
      const fakeItems = {
        fresh: ['test'],
        bakery: ['fake'],
      };
      const fakeSetAction = {
        type: SET_ITEMS,
        items: fakeItems,
      };

      state = itemsReducer(state, fakeSetAction);

      expect(state.items).toEqual(fakeItems);
    });

    it('Should update the state correctly on ADD_TO_SORT', () => {
      const fakeAction = {
        type: ADD_TO_SORT,
        item: 'fake',
        id: 0,
      };

      state = itemsReducer(state, fakeAction);

      expect(state.toSort[fakeAction.id]).toEqual(fakeAction.item);
    });

    it('Should update the state correctly on REMOVE_TO_SORT', () => {
      const fakeAction = {
        type: REMOVE_TO_SORT,
        id: 0,
      };

      state = itemsReducer(state, fakeAction);

      expect(!!state.toSort[fakeAction.id]).toBe(false);
    });

    it('Should update the state correctly on SET_ITEMS_STATE', () => {
      const fakeAddAction = {
        type: SET_ITEMS_STATE,
        state: initialState,
      };

      state = itemsReducer(state, fakeAddAction);

      expect(state).toEqual(initialState);
    });
  });
});
