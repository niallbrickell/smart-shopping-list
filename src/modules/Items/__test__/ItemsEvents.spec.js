import { addItem } from '../ItemsEvents';

describe('(Modules)', () => {
  describe('(Items - Events)', () => {
    it('Should export addItem as a function.', () => {
      expect(typeof addItem).toBe('function');
    });

    it('Should dispatch an action when addItem is called.', async () => {
      const fakeDispatch = jest.fn();

      await addItem('fake')(fakeDispatch);

      expect(fakeDispatch).toHaveBeenCalledTimes(3);
    });
  });
});
