import { setItems } from './SortableListActions';

/**
 * Called from the component.
 *
 * Nice to have here in case we want to do any processing logic that shouldn't
 *  be inside the component or action creator. Could leave out for a project
 *  of this size.
 */
export const updateState = items => setItems(items);
