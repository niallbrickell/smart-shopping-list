import React, { PropTypes } from 'react';
import { Sortable as sortable } from 'react-sortable';

/**
 * Used to enable dragging/sorting with react-sortable HOC.
 *
 * Could be in a seperate child module, but nice to keep things simple.
 */
const SortableListItem = props => (
  <div {...props}>{props.children}</div>
);

SortableListItem.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

export default sortable(SortableListItem);
