import { connect } from 'react-redux';

// -- Component --
import SortableListComponent from './SortableListComponent';

// -- Event handlers --
import { updateState } from './SortableListEvents';

// -- Connect react to redux --
const mapStateToProps = state => ({
  items: state.items.items,
});

const mapDispatchToProps = {
  updateState,
};

export default connect(mapStateToProps, mapDispatchToProps)(SortableListComponent);
