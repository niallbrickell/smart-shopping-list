import { updateState } from '../SortableListEvents';
import { SET_ITEMS } from '../SortableListActions';

describe('(Modules)', () => {
  describe('(Items)', () => {
    describe('(SortableList - Events)', () => {
      it('Should return a correct action when setItems is called.', () => {
        const fakeItems = {
          test: ['test'],
        };

        const returned = updateState(fakeItems);
        expect(typeof returned).toBe('object');
        expect(returned.type).toBe(SET_ITEMS);
        expect(returned.items).toEqual(fakeItems);
      });
    });
  });
});
