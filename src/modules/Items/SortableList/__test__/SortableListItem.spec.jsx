import React from 'react';
import { shallow } from 'enzyme';
import { ListItem } from 'material-ui/List';

import SortableListItem from '../SortableListItem';

describe('(Modules)', () => {
  describe('(Items)', () => {
    describe('(SortableList - SortableListItem)', () => {
      const itemText = 'test';

      let wrapper;
      beforeEach(() => {
        wrapper = shallow(
          <SortableListItem>{itemText}</SortableListItem>
        );
      });

      it('Should render a div with text.', () => {
        expect(
          wrapper.findWhere(n => n.text() === itemText).length
        ).toBe(1);
      });
    });
  });
});

