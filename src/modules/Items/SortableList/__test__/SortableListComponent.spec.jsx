import React from 'react';
import { shallow } from 'enzyme';
import { List, ListItem } from 'material-ui/List';

import SortableListComponent from '../SortableListComponent';

describe('(Modules)', () => {
  describe('(Items)', () => {
    describe('(SortableList - Component)', () => {
      const items = {
        fresh: ['oranges', 'pears', 'potatoes'],
        bakery: ['baguette', 'cookies'],
      };
      const locationsLength = Object
        .keys(items)
        .length;
      const updateState = () => null;

      let wrapper;
      beforeEach(() => {
        wrapper = shallow(
          <SortableListComponent
            items={items}
            updateState={updateState}
          />
        );
      });

      it("Should render an amount of List's.", () => {
        expect(
          wrapper.find(List).length
        ).toBe(locationsLength);
      });

      it("Should render an amount of ListItems's.", () => {
        expect(
          wrapper.find(ListItem).length
        ).toBe(locationsLength);
      });
    });
  });
});

