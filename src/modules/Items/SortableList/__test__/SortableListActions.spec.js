import { setItems, SET_ITEMS } from '../SortableListActions';

describe('(Modules)', () => {
  describe('(Items)', () => {
    describe('(SortableList - Events)', () => {
      it('Should return a correct action when setItems is called.', () => {
        const fakeItems = {
          test: ['test'],
        };

        const returned = setItems(fakeItems);
        expect(typeof returned).toBe('object');
        expect(returned.type).toBe(SET_ITEMS);
        expect(returned.items).toEqual(fakeItems);
      });
    });
  });
});
