/**
 * Public API for the SortableList sub-module.
 *
 * We use public APIs to limit access to our module.
 * No file can import a dependency from SortableList that isn't listed here.
 * Modules can consist of a component (with child components if required), or
 *  a reducer, logic etc.
 */

// -- Component --
export { default } from './SortableListContainer';

// -- Actions & Types --
export { SET_ITEMS } from './SortableListActions';
