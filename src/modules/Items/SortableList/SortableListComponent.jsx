import React, { PropTypes } from 'react';
import { List, ListItem } from 'material-ui/List';
import { blue500, red500, grey500 } from 'material-ui/styles/colors';

import SortableListItem from './SortableListItem';
import classes from './SortableList.scss';

// Helper to convert a str to title/first letter as as cap case
const toTitleCase = str => str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();

// Helper to get the representative color for a location
const getLocationColor = (location) => {
  switch (location.toLowerCase()) {
    case 'fresh':
      return blue500;
    case 'bakery':
      return red500;
    default:
      return grey500;
  }
};

/**
 * Component that renders and keeps track of the sortable shopping lists
 */
export default class SortableListComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: props.items,
      draggingIndex: null,
    };
  }

  /**
   * Updates the state from incoming props.
   *
   * Used to keep the selectable list and any addition/removal of items in sync.
   */
  componentWillReceiveProps(newProps) {
    this.setState({
      items: newProps.items,
    });
  }

  /**
   * Called from react-sortable.
   *
   * Updates the state from an items array, dragging index and
   *  a location key (a key of this.props.items).
   */
  updateState(stateObj, location) {
    let items = this.state.items;

    if (stateObj.items) {
      items = {
        ...this.state.items,
        [location]: stateObj.items,
      };

      this.props.updateState(items);
    }

    this.setState({
      items,
      draggingIndex: parseInt(stateObj.draggingIndex, 10),
    });
  }

  render() {
    const items = this.props.items;

    return (
      <div className={classes.sortableList}>
        {
          Object.keys(items).map((location, index) => {
            const captalisedLocation = toTitleCase(location);
            const locationColor = getLocationColor(location);

            return (
              <List
                key={index}
                className={classes.list}
              >
                <ListItem
                  primaryText={captalisedLocation}
                  initiallyOpen
                  primaryTogglesNestedList
                  style={{
                    color: 'b3b3b3',
                    borderBottom: `3px solid ${locationColor}`,
                  }}
                  nestedItems={
                    items[location].map((item, itemIndex) => (
                      <SortableListItem
                        key={`${index}${itemIndex}`}
                        updateState={state => this.updateState(state, location)}
                        items={items[location]}
                        draggingIndex={this.state.draggingIndex}
                        sortId={itemIndex}
                        outline="list"
                        className={classes.listItem}
                      >
                        <ListItem primaryText={item} />
                      </SortableListItem>
                    ))
                  }
                />
              </List>
            );
          })
        }
      </div>
    );
  }
}

SortableListComponent.propTypes = {
  items: PropTypes.shape({
    fresh: PropTypes.arrayOf(PropTypes.string),
    bakery: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,

  updateState: PropTypes.func.isRequired,
};
