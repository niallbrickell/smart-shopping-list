export const SET_ITEMS = 'SET_ITEMS';

/**
 * Sets items. Called when a list is sorted on a drag end.
 *
 * @param { object } items - the updated (/sorted) items object
 */
export const setItems = items => ({
  type: SET_ITEMS,
  items,
});
