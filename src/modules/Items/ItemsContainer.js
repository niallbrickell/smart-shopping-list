import { connect } from 'react-redux';

// -- Component --
import ItemsComponent from './ItemsComponent';

// -- Connect react to redux --
const mapStateToProps = state => ({
  toSort: Object.keys(state.items.toSort).map(sortId => state.items.toSort[sortId]),
});

// Empty but nice to keep around to save slight confusion & easily see that there are no actions
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsComponent);
