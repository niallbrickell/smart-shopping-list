import request from 'superagent';
import {
  addItem as addItemAction,
  addToSort,
  removeToSort,
} from './ItemsActions';
import determineLocation from './DetermineLocationHelper';

/**
 * Determines which list to add the item to, then calls the addItem action.
 */
export const addItem = item => (dispatch) => {
  const id = Math.floor(1000 + (Math.random() * 9000));
  dispatch(addToSort(item, id));

  // Turn plurals into singulars
  let itemToSearch;
  if (item.toLowerCase().slice(-1) === 's') itemToSearch = item.toLowerCase().slice(0, -1);
  else itemToSearch = item;

  return new Promise((resolve, reject) =>
    request
      .get(`https://wordsapiv1.p.mashape.com/words/${itemToSearch}/typeOf`)
      .set('X-Mashape-Key', '5E9ZBdip6VmshBYxqpOwwl15lvEUp1EgezBjsnt1q6KBcd70dg')
      .set('Accept', 'application/json')
      .then((res) => {
        if (!res) {
          dispatch(addItemAction(item, 'uncategorised'));
          dispatch(removeToSort(id));
          console.error('No response from sorting API!'); // eslint-disable-line no-console
          return;
          // TODO: Sort item manually
        }

        const types = res.body.typeOf;
        const location = determineLocation(types);
        dispatch(addItemAction(item, location));
        dispatch(removeToSort(id));
        resolve();
      })
      .catch((error) => {
        dispatch(addItemAction(item, 'uncategorised'));
        dispatch(removeToSort(id));
        console.error(error); // eslint-disable-line no-console
        reject();
        return;
        // TODO: Sort item manually
      })
    );
};
