const associatedWords = {
  fresh: ['fruit', 'vegetable', 'plant', 'herb'],
  bakery: ['bake', 'bread', 'bun', 'roll', 'cake'],
};

export default (types) => {
  let location = 'uncategorised';

  // TODO: Make a lot more clever
  types.forEach(type =>
    Object.keys(associatedWords).forEach(locationKey =>
      associatedWords[locationKey].forEach((word) => {
        if (type.toLowerCase().indexOf(word.toLowerCase()) >= 0) {
          location = locationKey;
          // TODO: for loops and break
        }
      })
    )
  );

  return location;
};
