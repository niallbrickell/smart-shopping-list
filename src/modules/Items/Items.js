/**
 * Public API for the Items module.
 *
 * We use public APIs to limit access to our module.
 * No file can import a dependency from Items that isn't listed here.
 * Modules can consist of a component (with child components if required), or
 *  a reducer, logic etc.
 */

// -- Component --
export { default } from './ItemsContainer';

// -- Reducer --
export { default as ItemsReducer } from './ItemsReducer';

// -- Events --
export { addItem } from './ItemsEvents';

// -- Actions & Types --
export { setItems, loadData, SET_DIRTY } from './ItemsActions';
