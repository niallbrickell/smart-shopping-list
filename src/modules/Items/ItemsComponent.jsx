import React, { PropTypes } from 'react';

import SortableList from './SortableList/SortableList';
import ToSortList from './ToSort/ToSort';

import classes from './Items.scss';

/**
 * Container component to hold the item list & add container CSS.
 */
const ItemsComponent = ({
  toSort,
}) => (
  <div className={classes.items}>
    <div className={classes.toSortContainer}>
      <ToSortList toSort={toSort} />
    </div>
    <div className={classes.sortableListContainer}>
      <SortableList />
    </div>
  </div>
);

ItemsComponent.propTypes = {
  toSort: PropTypes.arrayOf(PropTypes.string),
};

export default ItemsComponent;
