import {
  ADD_ITEM,
  ADD_TO_SORT,
  REMOVE_TO_SORT,
  SET_ITEMS_STATE,
} from './ItemsActions';
import { SET_ITEMS } from './SortableList/SortableList';
/**
 * Action handlers for the reducing function.
 *
 * These make it neater & easier to add new reducers compared to a `switch` statement.
 */
const ACTION_HANDLERS = {
  [SET_ITEMS_STATE]: (state, action) => action.state,

  [ADD_ITEM]: (state, action) => ({
    ...state,
    items: {
      ...state.items,
      [action.location]: [...state.items[action.location], action.item],
    },
  }),

  [SET_ITEMS]: (state, action) => ({
    ...state,
    items: action.items,
  }),

  [ADD_TO_SORT]: (state, action) => ({
    ...state,
    toSort: {
      ...state.toSort,
      [action.id]: action.item,
    },
  }),

  [REMOVE_TO_SORT]: (state, action) => {
    const updatedState = JSON.parse(JSON.stringify(state));

    delete updatedState.toSort[action.id];

    return updatedState;
  },
};

const initialState = {
  items: {
    // Separated by location in the store
    fresh: [],
    bakery: [],
    uncategorised: [],
  },
  // Object with IDs so that we can remove the correct item later on
  toSort: {},
};

// -- Reducing function --
export default (state = initialState, action) => {
  if (!action || !action.type) return state;

  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};
