import callAPI from '../../utils/callAPI';

export const ADD_ITEM = 'ADD_ITEM';
export const ADD_TO_SORT = 'ADD_TO_SORT';
export const REMOVE_TO_SORT = 'REMOVE_TO_SORT';
export const SET_ITEMS_STATE = 'SET_ITEMS_STATE';
export const SET_DIRTY = 'SET_DIRTY';

const setItemsState = state => ({
  type: SET_ITEMS_STATE,
  state,
});

export const setDirty = () => ({
  type: SET_DIRTY,
});

export const loadData = token => (dispatch) => {
  callAPI('getListFromToken', { token })
    .then(({ data }) => {
      dispatch(setItemsState(data));
      dispatch(setDirty());
    })
    .catch(error => console.error(error)); // eslint-disable-line no-console
};

/**
 * Adds the item to the relevant list
 *
 * The logic to determine which list is done in ItemsEvents to keep these
 *  action creators simple & easy to read.
 */
export const addItem = (item, location) => ({
  type: ADD_ITEM,
  item,
  location,
});

/**
 * Adds an item to the toSort list
 */
export const addToSort = (item, id) => ({
  type: ADD_TO_SORT,
  item,
  id,
});

/**
 * Removes an item from the toSort list
 */
export const removeToSort = id => ({
  type: REMOVE_TO_SORT,
  id,
});
