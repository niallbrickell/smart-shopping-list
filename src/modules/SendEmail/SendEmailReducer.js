import {
  SET_TOKEN,
  OPEN_SEND_EMAIL,
  CLOSE_SEND_EMAIL,
  SET_EMAIL,
  OPEN_SNACKBAR,
  CLOSE_SNACKBAR,
} from './SendEmailActions';

const ACTION_HANDLERS = {
  [OPEN_SEND_EMAIL]: state => ({
    ...state,
    sendEmailOpen: true,
  }),

  [CLOSE_SEND_EMAIL]: state => ({
    ...state,
    sendEmailOpen: false,
  }),

  [SET_TOKEN]: (state, action) => ({
    ...state,
    token: action.token,
  }),

  [SET_EMAIL]: (state, action) => ({
    ...state,
    email: action.email,
  }),

  [OPEN_SNACKBAR]: state => ({
    ...state,
    snackbarOpen: true,
  }),

  [CLOSE_SNACKBAR]: state => ({
    ...state,
    snackbarOpen: false,
  }),
};

const initialState = {
  token: '',
  email: '',
  sendEmailOpen: false,
  snackbarOpen: false,
};

// -- Reducing function --
export default (state = initialState, action) => {
  if (!action || !action.type) return state;

  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
};
