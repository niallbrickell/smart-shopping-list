import React, { PropTypes } from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import EmailIcon from 'material-ui/svg-icons/communication/email';

import classes from './SendEmail.scss';

/**
 * Container component for the SendEmail modal
 */
const SendEmailComponent = ({
  sendEmailOpen,
  token,
  email,
  pristine,
  snackbarOpen,

  openSendEmail,
  closeSendEmail,
  sendEmail,
  emailChanged,
  closeSnackbar,
  checkForEnter,
}) => {
  const actions = [
    <FlatButton
      label="Cancel"
      primary
      onTouchTap={closeSendEmail}
    />,
    <FlatButton
      label="Send!"
      primary
      onTouchTap={sendEmail}
    />,
  ];

  return (
    <div className={classes.sendEmail}>
      <EmailIcon
        className={`${classes.openSendEmailIcon} ${pristine ? '' : classes.active}`}
        onClick={openSendEmail}
        color="white"
      />
      <Snackbar
        open={snackbarOpen}
        message="Email sent"
        autoHideDuration={4000}
        onRequestClose={closeSnackbar}
      />
      <Dialog
        title="Email list to yourself"
        actions={actions}
        modal={false}
        open={sendEmailOpen}
        onRequestClose={closeSendEmail}
      >
        Email your unique URL: {token ? `${location.protocol}//${location.host}/${token}` : 'Loading...'}<br />
        This link will be valid for 1 month.
        <TextField
          floatingLabelText="Email"
          type="email"
          value={email}
          onChange={emailChanged}
          fullWidth
          autoFocus
          onKeyPress={checkForEnter}
        />
      </Dialog>
    </div>
  );
};

SendEmailComponent.propTypes = {
  sendEmailOpen: PropTypes.bool.isRequired,
  token: PropTypes.string.isRequired,
  pristine: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired,
  snackbarOpen: PropTypes.bool.isRequired,

  openSendEmail: PropTypes.func.isRequired,
  closeSendEmail: PropTypes.func.isRequired,
  sendEmail: PropTypes.func.isRequired,
  emailChanged: PropTypes.func.isRequired,
  closeSnackbar: PropTypes.func.isRequired,
  checkForEnter: PropTypes.func.isRequired,
};

export default SendEmailComponent;
