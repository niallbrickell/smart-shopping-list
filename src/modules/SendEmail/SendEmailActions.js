export const SET_TOKEN = 'SET_TOKEN';
export const OPEN_SEND_EMAIL = 'OPEN_SEND_EMAIL';
export const CLOSE_SEND_EMAIL = 'CLOSE_SEND_EMAIL';
export const SET_EMAIL = 'SET_EMAIL';
export const OPEN_SNACKBAR = 'OPEN_SNACKBAR';
export const CLOSE_SNACKBAR = 'CLOSE_SNACKBAR';

export const openSnackbar = () => ({
  type: OPEN_SNACKBAR,
});

export const closeSnackbar = () => ({
  type: CLOSE_SNACKBAR,
});

export const setEmail = email => ({
  type: SET_EMAIL,
  email,
});

export const openSendEmail = () => ({
  type: OPEN_SEND_EMAIL,
});

export const closeSendEmail = () => ({
  type: CLOSE_SEND_EMAIL,
});

/**
 * Sets the token in the reducer.
 *
 * Called from SendEmailEvents
 */
export const setToken = token => ({
  type: SET_TOKEN,
  token,
});
