import { connect } from 'react-redux';

// -- Component --
import SendEmailComponent from './SendEmailComponent';

// -- Event handlers --
import {
  openSendEmail,
  sendEmail,
  emailChanged,
  checkForEnter,
} from './SendEmailEvents';

// -- Actions --
import {
  closeSendEmail,
  openSnackbar,
  closeSnackbar,
} from './SendEmailActions';

// -- Connect react to redux --
const mapStateToProps = state => ({
  token: state.sendEmail.token,
  email: state.sendEmail.email,
  sendEmailOpen: state.sendEmail.sendEmailOpen,
  snackbarOpen: state.sendEmail.snackbarOpen,
  pristine: state.addItem.pristine,
});

// Empty but nice to keep around to save slight confusion & easily see that there are no actions
const mapDispatchToProps = {
  sendEmail,
  openSendEmail,
  closeSendEmail,
  emailChanged,
  openSnackbar,
  closeSnackbar,
  checkForEnter,
};

export default connect(mapStateToProps, mapDispatchToProps)(SendEmailComponent);
