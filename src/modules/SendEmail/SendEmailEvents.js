import callAPI from '../../utils/callAPI';
import {
  setToken,
  openSendEmail as openSendEmailAction,
  closeSendEmail as closeSendEmailAction,
  setEmail,
  openSnackbar,
} from './SendEmailActions';

/**
 * Calls the API to store the user's shopping list and returns a token
 *  which can be used to access the list via the API method 'getListFromToken'.
 */
export const saveList = items => dispatch =>
  callAPI('storeList', { data: items })
    .then(({ token }) => {
      if (!token) return;
      dispatch(setToken(token));
    })
    .catch(error => console.error(error)); // eslint-disable-line no-console

/**
 * Calls the API method to send an email via Mailgun to the user.
 */
export const sendEmail = () => (dispatch, getState) => {
  const url = `${location.protocol}//${location.host}/${getState().sendEmail.token}`;
  const email = getState().sendEmail.email;

  callAPI('emailList', { url, email })
      .then(() => {
        dispatch(closeSendEmailAction());
        dispatch(openSnackbar());
      })
      // TODO: Show error
      .catch(error => console.error(error)); // eslint-disable-line no-console
};

export const checkForEnter = e => (dispatch) => {
  const keyCode = e.keyCode ? e.keyCode : e.which;
  if (keyCode === 13) {
    // -- Enter pressed - send the email --
    dispatch(sendEmail());
  }
};

export const emailChanged = e => setEmail(e.target.value);

/**
 * Calls the API to store the user's list when SendEmail is opened.
 */
export const openSendEmail = () => (dispatch, getState) => {
  dispatch(saveList(getState().items));
  dispatch(openSendEmailAction());
};
