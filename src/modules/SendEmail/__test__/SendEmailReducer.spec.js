import {
  SET_TOKEN,
  OPEN_SEND_EMAIL,
  CLOSE_SEND_EMAIL,
  SET_EMAIL,
  OPEN_SNACKBAR,
  CLOSE_SNACKBAR,
} from '../SendEmailActions';
import sendEmailReducer from '../SendEmailReducer';

describe('(Modules)', () => {
  describe('(SendEmail - Reducer)', () => {
    let state;

    const initialState = {
      token: '',
      email: '',
      sendEmailOpen: false,
      snackbarOpen: false,
    };

    it('Should export a function as default.', () => {
      expect(typeof sendEmailReducer).toBe('function');
    });

    it('Should return an initialState when no action or state is present.', () => {
      state = sendEmailReducer();

      expect(state).toEqual(initialState);
    });

    it('Should update the state correctly on SET_TOKEN', () => {
      const fakeToken = 'fake';
      const fakeSetAction = {
        type: SET_TOKEN,
        token: fakeToken,
      };

      state = sendEmailReducer(state, fakeSetAction);

      expect(state.token).toBe(fakeToken);
    });

    it('Should update the state correctly on OPEN_SEND_EMAIL', () => {
      const fakeOpenAction = {
        type: OPEN_SEND_EMAIL,
      };

      state = sendEmailReducer(state, fakeOpenAction);

      expect(state.sendEmailOpen).toBe(true);
    });

    it('Should update the state correctly on CLOSE_SEND_EMAIL', () => {
      const fakeCloseAction = {
        type: CLOSE_SEND_EMAIL,
      };

      state = sendEmailReducer(state, fakeCloseAction);

      expect(state.sendEmailOpen).toBe(false);
    });

    it('Should update the state correctly on SET_EMAIL', () => {
      const fakeEmail = 'fake@fakey.com';
      const fakeEmailAction = {
        type: SET_EMAIL,
        email: fakeEmail,
      };

      state = sendEmailReducer(state, fakeEmailAction);

      expect(state.email).toBe(fakeEmail);
    });

    it('Should update the state correctly on OPEN_SNACKBAR', () => {
      const fakeOpenAction = {
        type: OPEN_SNACKBAR,
      };

      state = sendEmailReducer(state, fakeOpenAction);

      expect(state.snackbarOpen).toBe(true);
    });

    it('Should update the state correctly on CLOSE_SNACKBAR', () => {
      const fakeCloseAction = {
        type: CLOSE_SNACKBAR,
      };

      state = sendEmailReducer(state, fakeCloseAction);

      expect(state.snackbarOpen).toBe(false);
    });
  });
});
