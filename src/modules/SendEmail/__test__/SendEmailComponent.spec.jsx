import React from 'react';
import { shallow } from 'enzyme';
import Dialog from 'material-ui/Dialog';
import Snackbar from 'material-ui/Snackbar';
import EmailIcon from 'material-ui/svg-icons/communication/email';

import SendEmailComponent from '../SendEmailComponent';

describe('(Modules)', () => {
  describe('(SendEmail - Component)', () => {
    let wrapper;
    beforeEach(() => {
      wrapper = shallow(
        <SendEmailComponent
          token=""
          email=""
          sendEmailOpen
          pristine
          snackbarOpen
          closeSnackbar={() => null}
          sendEmail={() => null}
          openSendEmail={() => null}
          closeSendEmail={() => null}
          emailChanged={() => null}
          checkForEnter={() => null}
        />
      );
    });

    it('Should render an EmailIcon link.', () => {
      expect(
        wrapper.containsMatchingElement(<EmailIcon />)
      ).toBe(true);
    });

    it('Should render a Snackbar link.', () => {
      expect(
        wrapper.find(Snackbar).length
      ).toBe(1);
    });

    it('Should render a Dialog.', () => {
      expect(wrapper.find(Dialog).length).toBe(1);
    });
  });
});
