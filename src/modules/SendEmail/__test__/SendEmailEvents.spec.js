import {
  saveList,
  openSendEmail,
  emailChanged,
  checkForEnter,
} from '../SendEmailEvents';
import {
  openSendEmail as openSendEmailAction,
  setEmail,
} from '../SendEmailActions';

const ENTER_KEYCODE = 13;

describe('(Modules)', () => {
  describe('(SendEmail - Events)', () => {
    it('Should export saveList as a function.', () => {
      expect(typeof saveList).toBe('function');
    });

    it('Should export emailChanged as a function.', () => {
      expect(typeof emailChanged).toBe('function');
    });

    it('Should dispatch when saveList is called.', async () => {
      const fakeItems = {
        test: ['fake'],
      };

      const fakeDispatch = jest.fn();

      await saveList(fakeItems)(fakeDispatch);

      expect(fakeDispatch.mock.calls.length).toBe(1);
    });

    it('Should dispatch when checkForEnter is called with an enter key.', async () => {
      const fakeEvent = {
        keyCode: ENTER_KEYCODE,
      };

      const fakeDispatch = jest.fn();

      checkForEnter(fakeEvent)(fakeDispatch);

      expect(fakeDispatch.mock.calls.length).toBe(1);
    });

    it('Should NOT dispatch when checkForEnter is called with an enter key.', async () => {
      const fakeEvent = {
        keyCode: 10,
      };

      const fakeDispatch = jest.fn();

      checkForEnter(fakeEvent)(fakeDispatch);

      expect(fakeDispatch.mock.calls.length).toBe(0);
    });

    it('Should dispatch when openSendEmail is called.', async () => {
      const getState = () => ({
        items: {
          test: ['fake'],
        },
      });

      const fakeDispatch = jest.fn();

      await openSendEmail()(fakeDispatch, getState);

      expect(fakeDispatch.mock.calls.length).toBe(2);
      expect(fakeDispatch).toBeCalledWith(openSendEmailAction());
    });

    it('Should return an action when emailChanged is called.', () => {
      const fakeEmail = 'fake@fakey.com';
      const fakeEvent = {
        target: {
          value: fakeEmail,
        },
      };

      const returned = emailChanged(fakeEvent);
      expect(returned).toEqual(setEmail(fakeEmail));
    });
  });
});
