import {
  setToken,
  openSendEmail,
  closeSendEmail,
  setEmail,
  openSnackbar,
  closeSnackbar,

  SET_TOKEN,
  OPEN_SEND_EMAIL,
  CLOSE_SEND_EMAIL,
  SET_EMAIL,
  OPEN_SNACKBAR,
  CLOSE_SNACKBAR,
} from '../SendEmailActions';

describe('(Modules)', () => {
  describe('(SendEmail - Actions)', () => {
    it('Should export setToken as a function.', () => {
      expect(typeof setToken).toBe('function');
    });

    it('Should export openSendEmail as a function.', () => {
      expect(typeof openSendEmail).toBe('function');
    });

    it('Should export closeSendEmail as a function.', () => {
      expect(typeof closeSendEmail).toBe('function');
    });

    it('Should export setEmail as a function.', () => {
      expect(typeof setEmail).toBe('function');
    });

    it('Should export openSnackbar as a function.', () => {
      expect(typeof openSnackbar).toBe('function');
    });

    it('Should export closeSnackbar as a function.', () => {
      expect(typeof closeSnackbar).toBe('function');
    });

    it('Should return a correct action when setToken is called.', () => {
      const fakeToken = 'fake';

      const returned = setToken(fakeToken);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(SET_TOKEN);
      expect(returned.token).toBe(fakeToken);
    });

    it('Should return a correct action when openSendEmail is called.', () => {
      const returned = openSendEmail();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(OPEN_SEND_EMAIL);
    });

    it('Should return a correct action when closeSendEmail is called.', () => {
      const returned = closeSendEmail();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(CLOSE_SEND_EMAIL);
    });

    it('Should return a correct action when setEmail is called.', () => {
      const fakeEmail = 'fake@fakey.com';

      const returned = setEmail(fakeEmail);
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(SET_EMAIL);
      expect(returned.email).toBe(fakeEmail);
    });

    it('Should return a correct action when openSnackbar is called.', () => {
      const returned = openSnackbar();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(OPEN_SNACKBAR);
    });

    it('Should return a correct action when closeSnackbar is called.', () => {
      const returned = closeSnackbar();
      expect(typeof returned).toBe('object');
      expect(returned.type).toBe(CLOSE_SNACKBAR);
    });
  });
});
