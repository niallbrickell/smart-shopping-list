/**
 * Public API for the SendEmail module.
 *
 * We use public APIs to limit access to our module.
 * No file can import a dependency from SendEmail that isn't listed here.
 * Modules can consist of a component (with child components if required), or
 *  a reducer, logic etc.
 */

// -- Main component --
export { default } from './SendEmailContainer';

// -- Reducer --
export { default as SendEmailReducer } from './SendEmailReducer';

// -- Actions & Types --
export { openSendEmail } from './SendEmailActions';
