import request from 'superagent';
import config from '../config.json';

export default function callAPI(method, data) {
  if (!data) {
    return new Promise((resolve, reject) => {
      request
        .get(`${config.apiBaseURL}/call/${method}`)
        .then(res => resolve(res.body))
        .catch(e => reject(e));
    });
  }

  return new Promise((resolve, reject) => {
    request
      .post(`${config.apiBaseURL}/call/${method}`)
      .send({ ...data })
      .then(res => resolve(res.body))
      .catch(e => reject(e));
  });
}
